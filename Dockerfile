FROM gradle:6.7.1-jdk8
ARG APP
ENV APP_NAME=${APP}
COPY devschool-front-app-server/build/libs/$APP ./
ENTRYPOINT java -jar $APP_NAME -P:ktor.backend.host=backend

